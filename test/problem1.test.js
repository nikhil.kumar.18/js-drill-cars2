const f1 = require("../problem1");
const inventory = require("../extra_val");


test("Should return an array containing car 33's details", () => {
	const result=f1(inventory,33);
    expect(result).toEqual({"car_year":2011,"car_make": "Jeep","car_model":"Wrangler"});
    
    console.log(`Car 33 is a ${result["car_year"]} ${result["car_make"]} ${result["car_model"]}`);
});
