
function f3(inventory) {
  let copy_inventory=[...inventory];


  for (let i = 0; i < copy_inventory.length; i++) {
    for (let j = i + 1; j < copy_inventory.length; j++) {
      if (copy_inventory[i]["car_model"].toUpperCase() > copy_inventory[j]["car_model"].toUpperCase()) {
        let temp = copy_inventory[i];
        copy_inventory[i] = copy_inventory[j];
        copy_inventory[j] = temp;
      }
    }
  }
  return copy_inventory;
}
console.log("");


module.exports = f3;
