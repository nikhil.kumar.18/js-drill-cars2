

function f2(inventory) {
  let last_car = inventory[inventory.length - 1];

  return `Last car is a ${last_car["car_make"]} ${last_car["car_model"]}`;
}

module.exports = f2;


